import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TableauStartupComponent } from './tableau-startup/tableau-startup.component';
import { ConsultantsComponent } from './consultants/consultants.component';

const routes: Routes = [
  // {path:'/', component: EleveComponent},
  {path:'startups', component: TableauStartupComponent},
  {path:'consultants', component: ConsultantsComponent},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
