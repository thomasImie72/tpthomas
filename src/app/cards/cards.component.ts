import { Component, OnInit } from '@angular/core';
import { Consultant, Startup } from '../model';
import { StoreService } from '../store.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  tableauConsultant: Consultant[];
  tableauStartup: Startup[];

  constructor(private storeService: StoreService) { }

  ngOnInit() {
    this.storeService.listConsultants()
      .subscribe(
        (x: Array<Consultant>) => this.tableauConsultant = x,
        err => console.error('Observer got an error: ' + err),
        () => console.log('Observer got a complete notification')
      );

      this.storeService.listStartup()
      .subscribe(
        (x: Array<Startup>) => this.tableauStartup = x,
        err => console.error('Observer got an error: ' + err),
        () => console.log('Observer got a complete notification')
      );
  }

}
