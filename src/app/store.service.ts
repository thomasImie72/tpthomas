import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Startup } from './model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(private http: HttpClient) { }

  listStartup() {
    return this.http.get('http://toto/api/startup'); 
  }

  listConsultants() {
    return this.http.get('http://toto/api/consultant'); 
  }

  addStartup(startup: Startup): Observable<Startup> {
    return this.http.post<Startup>('http://toto/api/startup', startup);
  }

  delete(id){
    return this.http.delete(`http://toto/api/startup/${id}`);
  }
}
