import { Component, OnInit, Input } from '@angular/core';
import { StoreService } from '../store.service';
import { Startup } from '../model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-tableau-startup',
  templateUrl: './tableau-startup.component.html',
  styleUrls: ['./tableau-startup.component.css']
})
export class TableauStartupComponent implements OnInit {

  // @Input() startup: Startup;

  tableauStartup: Startup[];
  ajoutStartup: boolean = false;

  nom: FormControl;
  secteurActivite: FormControl;
  representantLegal: FormControl;
  nbreCoFondat: FormControl;
  description: FormControl;
  adresse: FormControl;

  startupForm: FormGroup;

  constructor(fb: FormBuilder, private storeService: StoreService) {
    this.nom = fb.control('', [Validators.required, Validators.maxLength(20)]);
    this.secteurActivite = fb.control('', [Validators.required, Validators.maxLength(10)]);
    this.representantLegal = fb.control('', [Validators.required, Validators.maxLength(15)]);
    this.nbreCoFondat = fb.control('', Validators.required);
    this.description = fb.control('', [Validators.required, Validators.maxLength(250)]);
    this.adresse = fb.control('', [Validators.required, Validators.maxLength(25)]);
    this.startupForm = fb.group({
      nom: this.nom,
      secteurActivite: this.secteurActivite,
      representantLegal: this.representantLegal,
      nbreCoFondat: this.nbreCoFondat,
      description: this.description,
      adresse: this.adresse
    });
  }

  ngOnInit() {
    this.storeService.listStartup()
      .subscribe(
        (x: Array<Startup>) => this.tableauStartup = x,
        err => console.error('Observer got an error: ' + err),
        () => console.log('Observer got a complete notification')
      );
  }

  ajouterStartup() {
    this.ajoutStartup = true;
  }

  register() {
    this.storeService.addStartup(this.startupForm.value).subscribe(
      x => this.tableauStartup.push(x),
      err => console.error('Observer got an error: ' + err),
      () => console.log('Observer got a complete notification')
    );
  }

  reset() {
    this.nom.setValue('');
    this.secteurActivite.setValue('');
    this.representantLegal.setValue('');
    this.nbreCoFondat.setValue('');
    this.description.setValue('');
    this.adresse.setValue('');
  }

  updateStartup() {

  }

  deleteStartup(id) {
    this.storeService.delete(id).subscribe(
      x => this.ngOnInit(),
      err => console.log(err),
      () => console.log('Observer got a complete notification')
    );
  }

}
