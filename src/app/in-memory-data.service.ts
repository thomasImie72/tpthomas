import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Startup, Consultant } from './model';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    const startup = [
      new Startup(1, 'Shopopop', 'Livraison', 'Antoine Cheul', 2, 'Startup nantaise', 'Ecole des mines à Nantes'),
      new Startup(2, 'Blablacar', 'Transport', 'Paul Bellot', 1, 'covoiturage'),
      new Startup(3, 'Smile', 'Service', 'Damien Foucault', 4, 'aide entre voisins'),
      new Startup(4, 'Shazam', 'Musique', 'Tiffany VanDyke', 1, 'reconaissance musicale', '8315 Fairmont Dr NW Albuquerque, NM'),
      new Startup(5, 'AllenMail', 'Agence web', 'Frederic Allender', 2, 'agence web mancelle', 'Gare Sud 72000 Le Mans')
    ];
    
    const consultant = [
      new Consultant(1, 'André', 'Valentin', 'Developpeur c#'),
      new Consultant(2, 'Alfonso', 'Micael', 'Developpeur Php'),
      new Consultant(3, 'Lefeuvre', 'Léa', 'Developpeuse Ios'),
      new Consultant(4, 'Lemaitre', 'Thomas', 'Dev ionic'),
      new Consultant(5, 'Legal', 'Pauline', 'Dev angular')
    ];

    return { startup, consultant};
  }

  genId<T extends Startup | Consultant>(table: T[]): number {
    // return eleves.length > 0 ? Math.max(...eleves.map(eleve => eleve.id)) + 1 : 11;
    return table.length > 0 ? Math.max(...table.map(t => t.id)) + 1 : 11;
  }
}
