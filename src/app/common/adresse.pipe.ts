import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'adresse'
})
export class AdressePipe implements PipeTransform {

  transform(value: string): string {
    return value ? 'OUI' : 'NON';
  }

}
