import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coFondateurs'
})
export class CoFondateursPipe implements PipeTransform {

  transform(value: number): string {
    switch (value) {
      case 1:
        return 'unique';
      case 2:
        return 'couple';
      default:
        return 'groupe';
    }
  }

}
