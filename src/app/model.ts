export class Startup {
    id: number;
    nom: string;
    secteurActivite: string;
    representantLegal: string;
    nbreCoFond: number;
    description: string;
    adresse: string;

    constructor(id, nom, secteurActivite, representantLegal, nbreCoFond, description, addresse?) {
            this.id = id;
            this.nom = nom; 
            this.secteurActivite = secteurActivite;
            this.representantLegal = representantLegal;
            this.nbreCoFond = nbreCoFond;
            this.description = description;
            this.adresse = addresse;
    }
}

export class Consultant {

    id: number;
    nom: string;
    prenom: string;
    description: string;

    constructor(id, nom, prenom, description) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.description = description;
    }
}