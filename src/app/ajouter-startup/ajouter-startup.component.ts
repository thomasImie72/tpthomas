import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { StoreService } from '../store.service';
import { Startup } from '../model';

@Component({
  selector: 'app-ajouter-startup',
  templateUrl: './ajouter-startup.component.html',
  styleUrls: ['./ajouter-startup.component.css']
})
export class AjouterStartupComponent implements OnInit {

  @Output() startUpUpdated: EventEmitter<Startup> = new EventEmitter();

  ajoutStartup: boolean = false;

  nomStartup: FormControl;
  secteurActivite: FormControl;
  represLegal: FormControl;
  nbreCoFondat: FormControl;
  description: FormControl;
  adresse: FormControl;

  startupForm: FormGroup;

  constructor(fb: FormBuilder, private storeService: StoreService) {
    this.nomStartup = fb.control('', [Validators.required, Validators.maxLength(20)]);
    this.secteurActivite = fb.control('', [Validators.required, Validators.maxLength(10)]);
    this.represLegal = fb.control('', [Validators.required, Validators.maxLength(15)]);
    this.nbreCoFondat = fb.control('', Validators.required);
    this.description = fb.control('', [Validators.required, Validators.maxLength(250)]);
    this.adresse = fb.control('');
    this.startupForm = fb.group({
      nomStartup: this.nomStartup,
      secteurActivite: this.secteurActivite,
      represLegal: this.represLegal,
      nbreCoFondat: this.nbreCoFondat,
      description: this.description,
      adresse: this.adresse
    });
  }

  ngOnInit() {
  }

  ajouterStartup() {
    this.ajoutStartup = true;
  }

  register() {
    this.storeService.addStartup(this.startupForm.value).subscribe(
      x => this.startUpUpdated.emit(x),
      err => console.error('Observer got an error: ' + err),
      () => console.log('Observer got a complete notification')
    );
  }

}
