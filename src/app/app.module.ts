import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { TableauStartupComponent } from './tableau-startup/tableau-startup.component';
import { CoFondateursPipe } from './common/co-fondateurs.pipe';
import { AdressePipe } from './common/adresse.pipe';
import { AjouterStartupComponent } from './ajouter-startup/ajouter-startup.component';
import { AppRoutingModule } from './/app-routing.module';
import { ConsultantsComponent } from './consultants/consultants.component';
import { CardsComponent } from './cards/cards.component';

@NgModule({
  declarations: [
    AppComponent,
    TableauStartupComponent,
    CoFondateursPipe,
    AdressePipe,
    AjouterStartupComponent,
    ConsultantsComponent,
    CardsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }),
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
