import { Component, OnInit } from '@angular/core';
import { StoreService } from '../store.service';
import { Startup, Consultant } from '../model';

@Component({
  selector: 'app-consultants',
  templateUrl: './consultants.component.html',
  styleUrls: ['./consultants.component.css']
})
export class ConsultantsComponent implements OnInit {

  tableauConsultant: Consultant[];

  constructor(private storeService: StoreService) { }

  ngOnInit() {
    this.storeService.listConsultants()
      .subscribe(
        (x: Array<Consultant>) => this.tableauConsultant = x,
        err => console.error('Observer got an error: ' + err),
        () => console.log('Observer got a complete notification')
      );
  }

}
